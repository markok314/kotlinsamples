
import kotlinx.coroutines.*
import kotlin.coroutines.*

suspend fun suspendingFunction(): String {
    var s = "suspendingFunction() before delay." + Thread.currentThread().name
    delay(1000L) // non-blocking delay for 1 second (default time unit is ms)
    s += "suspendingFunction() in the middle. " + Thread.currentThread().name
    delay(1000L)
    s += "suspendingFunction() after delay." + Thread.currentThread().name
    return s
}

suspend fun CoroutineScope.slowFunction(): String {
    println("\nMy context is: $coroutineContext")  // coroutineContext is visible in every coroutine
                                                   // It is the same object as the one in CoroutineScope.
    println("\nMy job is: ${coroutineContext[Job]}")
    val coroutineContext = 0                       // We can redefine it, but that's not recommended
    var s = "Before X delay. Thread: " + Thread.currentThread().name + ", context: $coroutineContext"
    println(s)
    delay(1000L) // non-blocking delay for 1 second (default time unit is ms)
    s = "In the middle. Thread: " + Thread.currentThread().name
    println(s)
    delay(1000L)
    s = "After delay. Thread: " + Thread.currentThread().name
    println(s)
    return s
}

suspend fun CoroutineScope.doesNotSuspend() {
    println("Does not call suspending function.")
}


fun CoroutineScope.fastFunction() {
    println("Before delay f.")
    Thread.sleep(100L)
    println("After delay f.")
}

class MyContinuation : Continuation<String> {
    override val context: CoroutineContext
        get() = EmptyCoroutineContext  // TODO("Not yet implemented")

    // This function is called with result of coroutine or an error.
    override fun resumeWith(result: Result<String>) {
        println("Continuation result: $result")
        // TODO("Not yet implemented")
    }

}

fun f() {

}


fun main() {

    // Interesting, but compiler does not complain if launch() method is given
    // non suspending function. fastFunction() is called, but no coroutine
    // machinery seems to be involved.
    GlobalScope.launch(block = CoroutineScope::fastFunction)
    GlobalScope.launch(block = CoroutineScope::slowFunction)
    GlobalScope.launch(block = CoroutineScope::doesNotSuspend)

    // lambda passed to launch() below is also a coroutine - it is
    // transformed by compiler to switch statement.
    GlobalScope.launch {
        print("main 1")
        suspendingFunction()
        print("main 2")
    }

    // 'Manually' create a coroutine and execute it.
    val cont1: Continuation<Unit> = CoroutineScope::slowFunction.createCoroutine(GlobalScope,
                                                                                 MyContinuation())
    cont1.resume(Unit)

    // Some function reference demo. Note '::' before function name.
    val fptr: () -> Unit = ::f
    val ptr: suspend () -> String = ::suspendingFunction

    // Create coroutine to run a standalone function.
    val context: CoroutineContext = EmptyCoroutineContext
    ::suspendingFunction.createCoroutine(Continuation(context){ result -> println() })
    ::suspendingFunction.createCoroutine(MyContinuation())

    println("main thread")
    Thread.sleep(2000L) // block main thread for 2 seconds to keep JVM alive
}

/**
 * @startuml
 * coroutine -> suspend: do()
 * @enduml
 */
/*
fun main_a() {

    GlobalScope.launch { // launch a new coroutine in background and continue
        println("Before delay.")
        delay(1000L) // non-blocking delay for 1 second (default time unit is ms)
        println("After delay.")
    }

    println("main thread")
    Thread.sleep(2000L) // block main thread for 2 seconds to keep JVM alive
}

fun main1() {
    val scope = CoroutineScope(Dispatchers.Default)

    scope.launch {
        // 2. do some cpu bound operations, runs on Default thread pool
        println("'${Thread.currentThread().name}': doing CPU work...")

        // 3. shifts to IO thread pool
        try {
            withContext(Dispatchers.IO) {
                // 4. do some io operations like file read, network calls  etc
                println("'${Thread.currentThread().name}': doing IO work...")
                throw IllegalArgumentException("moa izjema")
            }
        } catch (ex: Exception) {
            println("ujeta izjema")
        }

        // 5. shifts back to Default thread pool
        println("'${Thread.currentThread().name}': back to doing CPU work...")
    }

    Thread.sleep(3000)
    println("ended")
}



fun main_1() = runBlocking {

    this.launch {  // this is method on the same scope runBlocking() is running, so 
              // runBlocking will wait for this scope to complete
        // 2. do some cpu bound operations, runs on Default thread pool
        println("'${Thread.currentThread().name}': doing CPU work...")

        // 3. shifts to IO thread pool
        try {
        withContext(Dispatchers.IO) {
            // 4. do some io operations like file read, network calls  etc
            println("'${Thread.currentThread().name}': doing IO work...")
            throw IllegalArgumentException("moa izjema")
        }
        } catch (ex: Exception) {
            println("ujeta izjema")
        }

        // 5. shifts back to Default thread pool
        println("'${Thread.currentThread().name}': back to doing CPU work...")
    }

    println("ended")
}
*/