Kotlin startup guide
======================

Most Kotlin guides start with project in Android. This is not one of
them. It starts with "Hello world" built manually in command line, and
then gradually adds more features. The main goal of this guide is not
demonstration of all language features, because you can find many good
descriptions on the internet. This article describes features, which are
not that well explained or one has to put together information from
several sources to get clear picture about the topic.

Hello world
-----------

Let's start with the most basic example. Create file `HelloWorld.kt`:

    fun main() {
        println("Hello World!") 
    }

Although we can name file any way we want, Kotlin [coding conventions](https://kotlinlang.org/docs/reference/coding-conventions.html)
require upper camel case name. It is highly recommended to read coding 
conventions, and even more to stick to them. Especially this one:

> In Kotlin, semicolons are optional, and therefore line breaks are 
> significant. The language design assumes Java-style braces, and you may 
> encounter surprising behavior if you try to use a different formatting style.

To compile:

    $ kotlinc HelloWorld.kt

Compiler output is file `HelloWorldKt.class` and directory META-INF, which 
contains file [`main.kotlin_module`](https://stackoverflow.com/questions/45232350/disable-meta-inf-generation-in-gradle-android-library-kotlin-project).
To run the program type:

    $ kotlin HelloWorldKt

Next we'll a package name:

    package si.example

    fun main() {
        println("Hello World!") 
    }

After compilation we notice that output class file is saved to directory 
with package name. So we start it as:

    $ kotlin si.example.HelloWorldKt

Let's add printing of command line arguments:

    package si.example

    fun main(args: Array<String>) {
        println("Hello World! ${args.size} arg(s): ${args.joinToString()}") 
    }

To run:

    $ kotlin si.example.HelloWorldKt Hello Again


The following change makes our process exit with value 1:

    package si.example

    import kotlin.system.exitProcess


    fun main(args: Array<String>) {
        println("Hello World! ${args.size} arg(s): ${args.joinToString()}") 

        if (args.size < 3) {
            println("Error: At least 3 arguments are expected!")
            exitProcess(1)
        }
    }

Since JVM specifies, that main() has to return void (or nothing), we have to use 
function `exitProcess()`. This way any thread can exit process, 
not only the thread which started `main()`. If `main()` returns normally, exit code
0 is returned to the operating system. 

To call `exitProcess()` we had to add also `import` statement, which tells
the compiler the name of the package which contains the function.

Highly recommended reading:
https://kotlinlang.org/docs/tutorials/command-line.html
Compiling, REPL, scripts: https://kotlinlang.org/docs/command-line.html

Some other useful invocations:

    $ kotlinc -help
    $ kotlinc hello.kt -include-runtime -d hello.jar  # pack into jar with kotlin libs
    $ java -jar hello.jar


Real application
================

In the first section we had only one source file. Since real world projects
usually consist of many files, compiling them one by one in command line is
not an option. To solve this problem, build tools were created. For Kotlin this
build tool is called Gradle. It is a very powerful (but also complex) tool, so
here I'll show only the most basic steps.

Create a project
----------------

The easiest way to start a command line project is asking gradle to create the
basic files:

    $ gradle init

It asks you several questions. Correct answer for the first question is 
`2 - application`, as `1 - basic` creates only build file without contents.
Answers to other questions are obvious.

The following files were created:

- build.gradle.kts - contains project build script. It adds plugins needed for 
                     build, package repository and libraries. Very good
                     description can be found here:
                     https://medium.com/@gabrielshanahan/a-deep-dive-into-an-initial-kotlin-build-gradle-kts-8950b81b214
  TL;DR:
  - Instead of `id` we can call function `kotlin`, for example `kotlin("jvm") version 1.4.22`
  org.jetbrains.kotlin:kotlin-stdlib-jdk8 - contains standard Kotlin library with
  Collections, Math, Regex, ... More info at https://medium.com/@mbonnin/the-different-kotlin-stdlibs-explained-83d7c6bf293

  - org.jetbrains.kotlin:kotlin-bom - it is there to align versions of kotlin SDK
                                    libraries like kotlin-stdlib, kotlin-test, ...
                                    
  - For artifact repositories, add mavenCentral before jcenter, since publishing 
  to mavenCentral is more controlled, and is therefore more secure.

- gradlew.bat - start script for gradle on Windows
- settings.gradle.kts - contains project name
- src - this directory contains Kotlin source files, resources, and test files
        and resources. Directory structure is default one for Gradle. It is 
        possible to have custom directory structure, but then we have to
        specify it in build.gradle.kts. Don't do this until you have strong
        reasons. All you'll get is some additional work.


Creating project with IntelliJ
==============================

File | New Project, select Kotlin, enter project name, select Console Application 
and SDK.

So far so good, but when you want to run it, the run configuration wizard does
not find main class, which is MainKt in our case. The reason is, that we have
to select package first (poizkus.main) as shown in the image below:



Coroutines
==========

Note: _I started this section is my playground for Kotlin coroutines. Later
I added bits of information I found useful. It is not finished yet._

Coroutines are not simple to understand. They include compiler magic, several
classes, many library functions and some new concepts. So you may ask, why bother?
Why not program the old way using threads and some existing threading library?
Because with coroutines the code is very simple to write, read and maintain.
They give us great power for mastering complex concurrency in a simple way.
Coroutines were designed to support many existing asynchronous APIs.
Some use cases:
- Asynchronous computation
- Asynchronous GUI
- Futures
- Generators

Links:
https://kotlinlang.org/docs/coroutines-guide.html
KotlinConf 2018 - Elizarov - Coroutines in Practice: https://youtu.be/a3agLJQ6vt8
KotlinConf 2018 - Exploring Coroutines in Kotlin by Venkat Subramaniam: https://www.youtube.com/watch?v=jT2gHPQ4Z1Q


What are coroutines?
--------------------
Coroutines are functions, which call other functions declared with keyword `suspend`.

    suspend fun suspendingFunction(): String {
        var s = "suspendingFunction() before delay." + Thread.currentThread().name
        delay(1000L) // non-blocking delay for 1 second (default time unit is ms)
        s += "suspendingFunction() in the middle. " + Thread.currentThread().name
        delay(1000L)
        s += "suspendingFunction() after delay." + Thread.currentThread().name
        return s
    }

    fun main() {

        launch {
            print("main 1")
            suspendingFunction()
            print("main 2")
        }
    }

When execution of coroutine reaches call to function declared with keyword 
`suspend`, the current execution thread is released, so it can execute other code - 
it is **not** blocked until the execution of the function finishes. What happens 
with the suspending function? It is usually executed by some other thread or sometimes by the 
same thread, depending on how we launch a coroutine.

How many coroutines are in the snippet above?
Answer: 2 
The first one is suspendingFunction() and the second one is lambda passed to
launch(). Both get transformed by the compiler as explained in the next section.


The magic
---------

According to what was said above, several questions arise:

1. How is it possible that thread stops in the middle of a function (coroutine),
and then starts execution of something else?

2. How does execution of the 
function (coroutine) later resumes where it stopped? 
   
3. How is function state (parameters, local variables) preserved? 
   
4. What happens with stack frame,
if the function (coroutine) stops execution but does not return (clears the stack),
but the thread continues execution of something else? 

The answer is magic performed by the compiler. Functions declared with suspend
keyword look like normal functions only in source code. However, during the
compilation process they are significantly changed by the compiler:

1. They get an additional parameter of type Continuation. This object stores
   current state of state machine, function parameters and local variables, so 
   everything that's normally stored on the stack.
   
2. Body of a function is changed to switch statement, where each case ends
   with calling suspend function. There are as many case blocks as there are
   calls to suspend functions from the suspending function.

3. Class ContinuationImpl is created inside the suspend function on the first call. 
   It contains members, which store function state (parameters and 
   local variables), result, and one additional variable used in state machine.

This is the decompiled code of the original program. It is not very intuitive,
and may not be 100% accurate (decompilation is not a trivial task), but the
main principles are clearly visible. Comments in the code are mine.

    import kotlin.Metadata;
    import kotlin.ResultKt;
    import kotlin.Unit;
    import kotlin.coroutines.Continuation;
    import kotlin.coroutines.CoroutineContext;
    import kotlin.coroutines.intrinsics.IntrinsicsKt;
    import kotlin.coroutines.jvm.internal.ContinuationImpl;
    import kotlin.jvm.functions.Function2;
    import kotlin.jvm.internal.Intrinsics;
    import kotlinx.coroutines.BuildersKt;
    import kotlinx.coroutines.CoroutineScope;
    import kotlinx.coroutines.CoroutineStart;
    import kotlinx.coroutines.DelayKt;
    import kotlinx.coroutines.GlobalScope;
    import org.jetbrains.annotations.NotNull;
    import org.jetbrains.annotations.Nullable;

    // This annotation is added to any Kotlin class and is not important for
    // understanding of coroutines. More info:
    // https://github.com/JetBrains/kotlin/blob/master/libraries/stdlib/jvm/runtime/kotlin/Metadata.kt__
    @Metadata(
       mv = {1, 4, 1},
       bv = {1, 0, 3},
       k = 2,
       d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a\u0006\u0010\u0000\u001a\u00020\u0001\u001a\n\u0010\u0002\u001a\u00020\u0001*\u00020\u0003\u001a\u0015\u0010\u0004\u001a\u00020\u0005*\u00020\u0003H\u0086@ø\u0001\u0000¢\u0006\u0002\u0010\u0006\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0007"},
       d2 = {"main", "", "fastFunction", "Lkotlinx/coroutines/CoroutineScope;", "slowFunction", "", "(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "3-coroutines"}
    )

    // Kotlin puts functions not defined inside class to class which has name
    // generated from file name, to be compatible with JVM.
    public final class CoroutinesKt {
       @Nullable

       // The function has additional parameter of type Continuation added by the compiler
       public static final Object slowFunction(@NotNull CoroutineScope var0, @NotNull Continuation var1) {
          Object $continuation;
          label27: {
             // it seems that the sign bit of 'label' is used as a flag do mark the first run of coroutine
             if (var1 instanceof <undefinedtype>) {
                $continuation = (<undefinedtype>)var1;
                if ((((<undefinedtype>)$continuation).label & Integer.MIN_VALUE) != 0) {
                   ((<undefinedtype>)$continuation).label -= Integer.MIN_VALUE;
                   break label27;
                }
             }

             // this is Continuation class generated by the compiler, which holds all
             // the information required by coroutine.
             $continuation = new ContinuationImpl(var1) {
                // $FF: synthetic field
                Object result;   
                int label;     // stores the value of the case block to be executed on next invocation
                Object L$0;    // stores function local variable between invocations ('s' in our case)

                // it seems this is where execution of suspend function starts
                @Nullable
                public final Object invokeSuspend(@NotNull Object $result) {
                   this.result = $result;
                   this.label |= Integer.MIN_VALUE;  // set the sign bit to mark the first run
                   return CoroutinesKt.slowFunction((CoroutineScope)null, this);
                }
             };
          }

          StringBuilder var10000;
          Thread var10001;
          String s;
          label22: {
             Object $result = ((<undefinedtype>)$continuation).result;
             Object var5 = IntrinsicsKt.getCOROUTINE_SUSPENDED();
             switch(((<undefinedtype>)$continuation).label) {
             case 0:
                ResultKt.throwOnFailure($result);
                var10000 = (new StringBuilder()).append("Before delay.");
                var10001 = Thread.currentThread();
                Intrinsics.checkNotNullExpressionValue(var10001, "Thread.currentThread()");
                s = var10000.append(var10001.getName()).toString();
                ((<undefinedtype>)$continuation).L$0 = s;
                ((<undefinedtype>)$continuation).label = 1;

                // If the suspend function does not suspend (for example, it alredy 
                // has data from file in a buffer)
                // then the execution continues immediately. Otherwise COROUTINE_SUSPENDED 
                // is returned. Delay() always suspends, of course, but other suspend
                // functions may not.
                if (DelayKt.delay(1000L, (Continuation)$continuation) == var5) {
                   return var5;
                }
                break;
             case 1:
                s = (String)((<undefinedtype>)$continuation).L$0;
                ResultKt.throwOnFailure($result);
                break;
             case 2:
                s = (String)((<undefinedtype>)$continuation).L$0;
                ResultKt.throwOnFailure($result);
                break label22;
             default:
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
             }

             var10000 = (new StringBuilder()).append(s).append("In the middle. ");
             var10001 = Thread.currentThread();
             Intrinsics.checkNotNullExpressionValue(var10001, "Thread.currentThread()");
             s = var10000.append(var10001.getName()).toString();
             ((<undefinedtype>)$continuation).L$0 = s;
             ((<undefinedtype>)$continuation).label = 2;
             if (DelayKt.delay(1000L, (Continuation)$continuation) == var5) {
                return var5;
             }
          }

          var10000 = (new StringBuilder()).append(s).append("After delay.");
          var10001 = Thread.currentThread();
          Intrinsics.checkNotNullExpressionValue(var10001, "Thread.currentThread()");
          s = var10000.append(var10001.getName()).toString();
          return s;
       }

       // function not declated as suspended was not changed.
       public static final void fastFunction(@NotNull CoroutineScope $this$fastFunction) {
          Intrinsics.checkNotNullParameter($this$fastFunction, "$this$fastFunction");
          String var1 = "Before delay f.";
          boolean var2 = false;
          System.out.println(var1);
          Thread.sleep(100L);
          var1 = "After delay f.";
          var2 = false;
          System.out.println(var1);
       }

       public static final void main() {
          BuildersKt.launch$default((CoroutineScope)GlobalScope.INSTANCE, (CoroutineContext)null, (CoroutineStart)null, (Function2)(new Function2() {
             // $FF: synthetic method
             // $FF: bridge method
             public Object invoke(Object var1, Object var2) {
                return this.invoke((CoroutineScope)var1, (Continuation)var2);
             }

             @Nullable
             public final Object invoke(@NotNull CoroutineScope p1, @NotNull Continuation continuation) {
                CoroutinesKt.fastFunction(p1);
                return Unit.INSTANCE;
             }
          }), 3, (Object)null);
          BuildersKt.launch$default((CoroutineScope)GlobalScope.INSTANCE, (CoroutineContext)null, (CoroutineStart)null, (Function2)(new Function2() {
             // $FF: synthetic method
             // $FF: bridge method
             public Object invoke(Object var1, Object var2) {
                return this.invoke((CoroutineScope)var1, (Continuation)var2);
             }

             @Nullable
             public final Object invoke(@NotNull CoroutineScope p1, @NotNull Continuation continuation) {
                return CoroutinesKt.slowFunction(p1, continuation);
             }
          }), 3, (Object)null);
          String var0 = "main thread";
          boolean var1 = false;
          System.out.println(var0);
          Thread.sleep(2000L);
       }

       // $FF: synthetic method
       public static void main(String[] var0) {
          main();
       }
    }

More detailed description can be found at:
https://www.infoq.com/articles/kotlin-coroutines-bottom-up/
https://stackoverflow.com/questions/53526556/how-do-kotlin-coroutines-work-internally
https://www.youtube.com/watch?v=jT2gHPQ4Z1Q
https://stackoverflow.com/questions/47871868/what-does-the-suspend-function-mean-in-a-kotlin-coroutine

Answers:
1. The thread does actually not exit in the middle of a function - it
   only exits case block in a switch statement, then normally exits the 
   function. Before the exit it increments the label.

2. Function state (local variables and parameters) is preserved in 
   instance of Continuation object generated by compiler.

2. On resume the continuation object also contains the information about which
   case block to execute, in addition to function state.
   
4. Stack frame is actually normally cleared on suspension point. Information
  is stored in the Continuation object.


Problems and solutions
----------------------

**Continuation**

    interface Continuation<in T> {
        val context: CoroutineContext
        fun resumeWith(result: Result<T>)
    }

However, there exists also a factory function `Continuation`, declared as:

    inline fun <T> Continuation(context: CoroutineContext,
                                crossinline resumeWith: (Result<T>) -> Unit
    ): Continuation<T>

which creates a Continuation instance with the given parameters.

**Context**

https://elizarov.medium.com/coroutine-context-and-scope-c8b255d59055

Coroutine context is a persistent set of user-defined objects that can be 
attached to the coroutine. For example:

- Dispatcher - object responsible for a coroutine threading policy
- logging
- security and transaction aspects of the coroutine execution,
- coroutine identity and name
- ...

Coroutine context's main elements are Job and Dispatchers. Coroutine dispatcher
determines what thread or threads the corresponding coroutine uses for its 
execution. The coroutine dispatcher can:

  - confine coroutine execution to a specific thread,
  - dispatch it to a thread pool, 
  - let it run unconfined.


**Dispatchers**

Dispatcher can be specified in parameter CoroutineContext, which is passed to
[coroutine builder] functions.

- For computationally intensive suspend functions we do not want to use
  more threads than the number of cores in our CPU, because it would make things
  only worse (more time spent on thread switching).

- For suspend functions, which spend time on IO calls, the number of threads
  is not limited with the number of cores.

Q: How do we tell Kotlin, whether it should create new threads or not for our
   suspending function?
A: By specifying a Coroutine Dispatcher.

Links:
About unconfined dispatcher: https://stackoverflow.com/questions/54695301/why-coroutines-1st-run-on-caller-thread-but-after-first-suspension-point-it-runs
https://kotlinlang.org/docs/coroutine-context-and-dispatchers.html#unconfined-vs-confined-dispatcher


TODO example

**Problem**

Q: How can we terminate execution of dependent coroutines if one of them fails?
Suppose that one coroutine detects error in input data, which makes all 
calculations invalid. It makes no sense for other coroutines to continue their
work, so we should somehow tell them to stop. Another example may be in
Android, when user swithces to the next fragment and we would like to stop all
coroutines related to this fragment.

A: By running all dependent coroutines in the same Coroutine Scope.

**Problem**

A: Launchers.

**Problem**

Q: What about when we want to wait for result of the coroutine or several of them?

A: await

Terminology:
- Continuation
- Context
  Contains Job, coroutine name and other items related to coroutine. As a
  data structure it is a mix between a set and a map, so new members can be
  added on the fly by libraries.
  Job is responsible for coroutine’s lifecycle, cancellation, and parent-child 
  relations.
  Context is immutable, but with '+' operator we can easily create new context
  with an additional element.

      val newContext = coroutineContext + CoroutineName("test")  

  CoroutineContext is member of CoroutineScope interface, but also given as
  parameter in coroutine builders. These tqo CoroutineContext-s are then 
  merged (with parameter overriding scope's context) and use as a parent context 
  of a new coroutine. Reading the Elizarov's article linked below is highly
  recommended.
  Links:
  https://elizarov.medium.com/coroutine-context-and-scope-c8b255d59055
  
  By different parametrization of context, we can achieve many different 
  concurrent behaviors, for example 'Cooperative single-thread multitasking':
  val context = newSingleThreadContext("MyEventThread")
  val f = future(context) { ... }
  Link: https://github.com/Kotlin/KEEP/blob/master/proposals/coroutines.md#cooperative-single-thread-multitasking
  
- Dispatcher
  Default dispatcher: we use the Default dispatcher to execute CPU-bound code. 
  The default dispatcher is optimized for such CPU-bound functions as it 
  is backed by a thread-pool with as many threads as there are CPU cores in 
  the system, making sure that CPU-bound code can saturate all physical 
  resources as needed. https://elizarov.medium.com/blocking-threads-suspending-coroutines-d33e11bf4761
  
- Scope
  GlobalScope - https://kotlinlang.org/docs/coroutines-basics.html#global-coroutines-are-like-daemon-threads
  The reason to avoid GlobalScope: https://elizarov.medium.com/the-reason-to-avoid-globalscope-835337445abc
    TL;DR: Since it is global scope, there is no structured concurrency.
  Scope builder of runBlocking() vs. coroutineScope(): https://kotlinlang.org/docs/coroutines-basics.html#scope-builder
  Composing suspending functions - Structured concurrency with async: https://kotlinlang.org/docs/composing-suspending-functions.html#structured-concurrency-with-async
- Suspending
- Job

- yield
  buildIterator, buildSequence: https://blog.drako.guru/2018/07/31/introduction-to-coroutines-part-2/
  buildSequence: https://www.baeldung.com/kotlin/coroutines

Which thread should execute the suspending function?

What happens, if we pass non-suspending function to coroutine builder?
----------------------------------------------------------------------

The function:

    fun CoroutineScope.fastFunction() {
        println("Before delay f.")
        Thread.sleep(100L) // non-blocking delay for 1 second (default time unit is ms)
        println("After delay f.")
    }

Invocation from main():

    GlobalScope.launch(block = CoroutineScope::fastFunction)

Generated code:

public static final void fastFunction(@NotNull CoroutineScope $this$fastFunction) {
    Intrinsics.checkNotNullParameter($this$fastFunction, "$this$fastFunction");
    String var1 = "Before delay f.";
    boolean var2 = false;
    System.out.println(var1);
    Thread.sleep(100L);
    var1 = "After delay f.";
    var2 = false;
    System.out.println(var1);
}

We can see, that the function is not modified - it doesn't get additional
parameter of type Continuation and there is no state machine. It works, but 
makes no sense to use coroutine launcher in such cases.


Coroutines and exceptions
-------------------------

Handling of exceptions depends on coroutine builder used. For example launch()
treats exceptions as uncaught exception, while async() throws it when await()
is called.

https://kotlinlang.org/docs/exception-handling.html#coroutineexceptionhandler

To build
--------

    $ gradle build

To run:

    $ tar xvf build/distributions/kotlinSamples.tar &&  ./kotlinSamples/bin/kotlinSamples CoroutinesKt
Links:
https://github.com/Kotlin/KEEP/blob/master/proposals/coroutines.md
https://en.wikipedia.org/wiki/Java_bytecode_instruction_listings
https://github.com/JetBrains/intellij-community/blob/master/plugins/java-decompiler/engine/README.md
https://kotlin.christmas/2020/5 - decompiler, see also IJ: Tools | Kotlin

Kotlin standard library for coroutines:
https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.coroutines/