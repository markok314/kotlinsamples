
package si.example

import kotlin.system.exitProcess


fun main(args: Array<String>) {
    println("Hello World! ${args.size} arg(s): ${args.joinToString()}") 

    if (args.size < 3) {
        println("Error: At least 3 arguments are expected!")
        exitProcess(1)
    }
}

